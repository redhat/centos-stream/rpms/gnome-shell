From cfeade9501e158b426f571d05ab15ee6dbe49e75 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Florian=20M=C3=BCllner?= <fmuellner@gnome.org>
Date: Fri, 7 Feb 2025 15:01:48 +0100
Subject: [PATCH 1/3] data: Generate dash/app-grid defaults from text files

Defining default apps as serialized GVariants isn't very human-friendly,
which likely contributes to the fact that the lists are in parts horribly
outdated (Books! Cheese! Screenshot! gedit!).

Instead, generate the lists at build time from simple text files, which
should be much easier to update.

Part-of: <https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3632>
---
 data/default-apps/app-grid.txt         | 21 ++++++++++++++
 data/default-apps/dash.txt             |  6 ++++
 data/default-apps/utilities-folder.txt | 17 +++++++++++
 data/meson.build                       |  8 ++++++
 data/org.gnome.shell.gschema.xml.in    | 28 +++---------------
 js/misc/config.js.in                   |  2 ++
 js/misc/meson.build                    |  4 +++
 js/ui/appDisplay.js                    | 19 ++----------
 meson.build                            |  1 +
 meson/generate-app-list.py             | 40 ++++++++++++++++++++++++++
 10 files changed, 106 insertions(+), 40 deletions(-)
 create mode 100644 data/default-apps/app-grid.txt
 create mode 100644 data/default-apps/dash.txt
 create mode 100644 data/default-apps/utilities-folder.txt
 create mode 100755 meson/generate-app-list.py

diff --git a/data/default-apps/app-grid.txt b/data/default-apps/app-grid.txt
new file mode 100644
index 0000000000..d78ed165eb
--- /dev/null
+++ b/data/default-apps/app-grid.txt
@@ -0,0 +1,21 @@
+org.gnome.Geary.desktop
+org.gnome.Contacts.desktop
+org.gnome.Weather.desktop
+org.gnome.clocks.desktop
+org.gnome.Maps.desktop
+org.gnome.Books.desktop
+org.gnome.Photos.desktop
+org.gnome.Totem.desktop
+org.gnome.Calculator.desktop
+org.gnome.gedit.desktop
+simple-scan.desktop
+org.gnome.Settings.desktop
+org.gnome.SystemMonitor.desktop
+org.gnome.Boxes.desktop
+org.gnome.Terminal.desktop
+Utilities # folder
+org.gnome.Characters.desktop
+yelp.desktop
+org.gnome.Screenshot.desktop
+org.gnome.Cheese.desktop
+org.gnome.font-viewer.desktop
diff --git a/data/default-apps/dash.txt b/data/default-apps/dash.txt
new file mode 100644
index 0000000000..2a8e8bcd06
--- /dev/null
+++ b/data/default-apps/dash.txt
@@ -0,0 +1,6 @@
+org.gnome.Epiphany.desktop
+org.gnome.Calendar.desktop
+org.gnome.Music.desktop
+gnome.Nautilus.desktop
+org.gnome.Software.desktop
+org.gnome.TextEditor.desktop
diff --git a/data/default-apps/utilities-folder.txt b/data/default-apps/utilities-folder.txt
new file mode 100644
index 0000000000..6169977751
--- /dev/null
+++ b/data/default-apps/utilities-folder.txt
@@ -0,0 +1,17 @@
+# Sorted by name as shown in menus, not filename
+nm-connection-editor.desktop              # Advanced Network Configuration
+org.gnome.DejaDup.desktop                 # Backups
+org.gnome.Characters.desktop              # Characters
+org.gnome.Connections.desktop             # Connections
+org.gnome.DiskUtility.desktop             # Disks
+org.gnome.baobab.desktop                  # Disk Usage Analyzer
+org.gnome.Evince.desktop                  # Document Viewer
+org.gnome.FileRoller.desktop              # File Roller
+org.gnome.font-viewer.desktop             # Fonts
+org.gnome.Loupe.desktop                   # Image Viewer
+org.gnome.Logs.desktop                    # Logs
+org.freedesktop.MalcontentControl.desktop # Parental Controls
+org.gnome.seahorse.Application.desktop    # Passwords and Keys
+org.freedesktop.GnomeAbrt.desktop         # Problem Reporting
+org.gnome.tweaks.desktop                  # Tweaks
+org.gnome.Usage.desktop                   # Usage
diff --git a/data/meson.build b/data/meson.build
index ed13b6baea..c7565ee810 100644
--- a/data/meson.build
+++ b/data/meson.build
@@ -90,6 +90,14 @@ install_data(keybinding_files, install_dir: keysdir)
 
 schemaconf = configuration_data()
 schemaconf.set('GETTEXT_PACKAGE', meson.project_name())
+schemaconf.set('DASH_APPS', run_command(
+  generate_app_list, 'default-apps/dash.txt',
+  check: true,
+).stdout())
+schemaconf.set('APP_GRID_APPS', run_command(
+  generate_app_list, '--pages', 'default-apps/app-grid.txt',
+  check: true,
+).stdout())
 schema = configure_file(
   input: 'org.gnome.shell.gschema.xml.in',
   output: 'org.gnome.shell.gschema.xml',
diff --git a/data/org.gnome.shell.gschema.xml.in b/data/org.gnome.shell.gschema.xml.in
index 3cd37692a7..b1c2534f9c 100644
--- a/data/org.gnome.shell.gschema.xml.in
+++ b/data/org.gnome.shell.gschema.xml.in
@@ -61,7 +61,9 @@
       </description>
     </key>
     <key name="favorite-apps" type="as">
-      <default>[ 'org.gnome.Epiphany.desktop', 'org.gnome.Calendar.desktop', 'org.gnome.Music.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Software.desktop', 'org.gnome.TextEditor.desktop']</default>
+      <default>
+        @DASH_APPS@
+      </default>
       <summary>List of desktop file IDs for favorite applications</summary>
       <description>
         The applications corresponding to these identifiers
@@ -118,29 +120,7 @@
     </key>
     <key name="app-picker-layout" type="aa{sv}">
       <default><![CDATA[
-        [{
-          'org.gnome.Geary.desktop': <{'position': <0>}>,
-          'org.gnome.Contacts.desktop': <{'position': <1>}>,
-          'org.gnome.Weather.desktop': <{'position': <2>}>,
-          'org.gnome.clocks.desktop': <{'position': <3>}>,
-          'org.gnome.Maps.desktop': <{'position': <4>}>,
-          'org.gnome.Books.desktop': <{'position': <5>}>,
-          'org.gnome.Photos.desktop': <{'position': <6>}>,
-          'org.gnome.Totem.desktop': <{'position': <7>}>,
-          'org.gnome.Calculator.desktop': <{'position': <8>}>,
-          'org.gnome.gedit.desktop': <{'position': <9>}>,
-          'simple-scan.desktop': <{'position': <10>}>,
-          'org.gnome.Settings.desktop': <{'position': <11>}>,
-          'org.gnome.SystemMonitor.desktop': <{'position': <12>}>,
-          'org.gnome.Boxes.desktop': <{'position': <13>}>,
-          'org.gnome.Terminal.desktop': <{'position': <14>}>,
-          'Utilities': <{'position': <15>}>,
-          'org.gnome.Characters.desktop': <{'position': <16>}>,
-          'yelp.desktop': <{'position': <17>}>,
-          'org.gnome.Screenshot.desktop': <{'position': <18>}>,
-          'org.gnome.Cheese.desktop': <{'position': <19>}>,
-          'org.gnome.font-viewer.desktop': <{'position': <20>}>
-        }]
+        @APP_GRID_APPS@
       ]]></default>
       <summary>Layout of the app picker</summary>
       <description>
diff --git a/js/misc/config.js.in b/js/misc/config.js.in
index a5069e438a..6485d614f5 100644
--- a/js/misc/config.js.in
+++ b/js/misc/config.js.in
@@ -20,3 +20,5 @@ export const LIBMUTTER_API_VERSION = '@LIBMUTTER_API_VERSION@';
 
 export const HAVE_BLUETOOTH = pkg.checkSymbol('GnomeBluetooth', '3.0',
     'Client.default_adapter_state');
+
+export const UTILITIES_FOLDER_APPS = @UTILS_FOLDER_APPS@;
diff --git a/js/misc/meson.build b/js/misc/meson.build
index 5fc8ca433f..4b137ef3e6 100644
--- a/js/misc/meson.build
+++ b/js/misc/meson.build
@@ -5,6 +5,10 @@ jsconf.set('GETTEXT_PACKAGE', meson.project_name())
 jsconf.set('LIBMUTTER_API_VERSION', mutter_api_version)
 jsconf.set10('HAVE_NETWORKMANAGER', have_networkmanager)
 jsconf.set10('HAVE_PORTAL_HELPER', have_portal_helper)
+jsconf.set('UTILS_FOLDER_APPS', run_command(
+  generate_app_list, '../../data/default-apps/utilities-folder.txt',
+  check: true,
+).stdout())
 jsconf.set('datadir', datadir)
 jsconf.set('libexecdir', libexecdir)
 
diff --git a/js/ui/appDisplay.js b/js/ui/appDisplay.js
index bdce3bdeed..992004b8f9 100644
--- a/js/ui/appDisplay.js
+++ b/js/ui/appDisplay.js
@@ -24,6 +24,8 @@ import * as SystemActions from '../misc/systemActions.js';
 
 import * as Main from './main.js';
 
+import {UTILITIES_FOLDER_APPS} from '../misc/config.js';
+
 const MENU_POPUP_TIMEOUT = 600;
 const POPDOWN_DIALOG_TIMEOUT = 500;
 
@@ -60,22 +62,7 @@ const DEFAULT_FOLDERS = {
     'Utilities': {
         name: 'X-GNOME-Utilities.directory',
         categories: ['X-GNOME-Utilities'],
-        apps: [
-            'org.freedesktop.GnomeAbrt.desktop',
-            'nm-connection-editor.desktop',
-            'org.gnome.baobab.desktop',
-            'org.gnome.Connections.desktop',
-            'org.gnome.DejaDup.desktop',
-            'org.gnome.DiskUtility.desktop',
-            'org.gnome.Evince.desktop',
-            'org.gnome.FileRoller.desktop',
-            'org.gnome.font-viewer.desktop',
-            'org.gnome.Loupe.desktop',
-            'org.freedesktop.MalcontentControl.desktop',
-            'org.gnome.seahorse.Application.desktop',
-            'org.gnome.tweaks.desktop',
-            'org.gnome.Usage.desktop',
-        ],
+        apps: UTILITIES_FOLDER_APPS,
     },
     'YaST': {
         name: 'suse-yast.directory',
diff --git a/meson.build b/meson.build
index c9955ca651..6f10a366bf 100644
--- a/meson.build
+++ b/meson.build
@@ -137,6 +137,7 @@ endif
 mutter_typelibdir = mutter_dep.get_variable('typelibdir')
 python = find_program('python3')
 gjs = find_program('gjs')
+generate_app_list = find_program('meson/generate-app-list.py')
 
 cc = meson.get_compiler('c')
 
diff --git a/meson/generate-app-list.py b/meson/generate-app-list.py
new file mode 100755
index 0000000000..067c36967c
--- /dev/null
+++ b/meson/generate-app-list.py
@@ -0,0 +1,40 @@
+#!/usr/bin/env python3
+
+import argparse
+
+def read_app_ids(path) -> str:
+    ids = []
+    with open(path, "r") as file:
+        for line in file:
+            # strip comments
+            line, _, _ = line.partition('#');
+            line = line.strip()
+            if len(line) > 0:
+                ids.append(line)
+    return ids
+
+def print_as_array(ids):
+    mapped_ids = list(map(lambda i: f"  '{i}'", ids))
+    print('[')
+    print(',\n'.join(mapped_ids))
+    print(']')
+
+def print_as_pages(ids):
+    mapped_ids = []
+    for i, id in enumerate(ids):
+        mapped_ids.append(f"  '{id}': <{{'position': <{i}>}}>")
+
+    print('[{')
+    print(',\n'.join(mapped_ids))
+    print('}]')
+
+parser = argparse.ArgumentParser()
+parser.add_argument('--pages', action='store_true')
+parser.add_argument('file')
+args = parser.parse_args()
+
+ids = read_app_ids(args.file)
+if args.pages:
+    print_as_pages(ids)
+else:
+    print_as_array(ids)
-- 
2.48.1


From aeb7adf061d74d75edc85f65520cb404e8a4361d Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Florian=20M=C3=BCllner?= <fmuellner@gnome.org>
Date: Fri, 14 Feb 2025 15:44:30 +0100
Subject: [PATCH 2/3] data: Tweak dash/app-grid defaults

With the current RHEL 10 app set, the dash and app grid look
rather empty.

Address this by
 - adding Calculator to dash
 - moving Characters and Baobab out of the Utilities folder

With those changes, there are at least 5 apps in the dash and 2 rows
in the app grid.
---
 data/default-apps/dash.txt             | 1 +
 data/default-apps/utilities-folder.txt | 2 --
 2 files changed, 1 insertion(+), 2 deletions(-)

diff --git a/data/default-apps/dash.txt b/data/default-apps/dash.txt
index 2a8e8bcd06..d0c2c21eb8 100644
--- a/data/default-apps/dash.txt
+++ b/data/default-apps/dash.txt
@@ -4,3 +4,4 @@ org.gnome.Music.desktop
 gnome.Nautilus.desktop
 org.gnome.Software.desktop
 org.gnome.TextEditor.desktop
+org.gnome.Calculator.desktop
diff --git a/data/default-apps/utilities-folder.txt b/data/default-apps/utilities-folder.txt
index 6169977751..187e3cfc54 100644
--- a/data/default-apps/utilities-folder.txt
+++ b/data/default-apps/utilities-folder.txt
@@ -1,10 +1,8 @@
 # Sorted by name as shown in menus, not filename
 nm-connection-editor.desktop              # Advanced Network Configuration
 org.gnome.DejaDup.desktop                 # Backups
-org.gnome.Characters.desktop              # Characters
 org.gnome.Connections.desktop             # Connections
 org.gnome.DiskUtility.desktop             # Disks
-org.gnome.baobab.desktop                  # Disk Usage Analyzer
 org.gnome.Evince.desktop                  # Document Viewer
 org.gnome.FileRoller.desktop              # File Roller
 org.gnome.font-viewer.desktop             # Fonts
-- 
2.48.1


From 7a21fca2f59d68896f54600b64e5d9a216203e9e Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?Florian=20M=C3=BCllner?= <fmuellner@gnome.org>
Date: Fri, 7 Feb 2025 15:54:15 +0100
Subject: [PATCH 3/3] appDisplay: Filter apps in default folder

We currently create the default folder with the corresponding
app list, regardless of whether the apps are actually part of
the default install or not.

This matters when a user explicitly install such an app later,
as it will be hidden away in the folder rather than appended
to the app grid as expected.

To avoid that, only add currently-installed apps to the folder
when creating it.

Part-of: <https://gitlab.gnome.org/GNOME/gnome-shell/-/merge_requests/3632>
---
 js/ui/appDisplay.js | 8 ++++++--
 1 file changed, 6 insertions(+), 2 deletions(-)

diff --git a/js/ui/appDisplay.js b/js/ui/appDisplay.js
index 992004b8f9..6bd2d899cd 100644
--- a/js/ui/appDisplay.js
+++ b/js/ui/appDisplay.js
@@ -1419,12 +1419,16 @@ class AppDisplay extends BaseAppView {
         if (this._folderSettings.get_strv('folder-children').length > 0)
             return;
 
+        const appSys = Shell.AppSystem.get_default();
         const folders = Object.keys(DEFAULT_FOLDERS);
         this._folderSettings.set_strv('folder-children', folders);
 
         const {path} = this._folderSettings;
         for (const folder of folders) {
             const {name, categories, apps} = DEFAULT_FOLDERS[folder];
+            const filteredApps = apps
+                ? apps.filter(id => appSys.lookup_app(id) != null)
+                : [];
             const child = new Gio.Settings({
                 schema_id: 'org.gnome.desktop.app-folders.folder',
                 path: `${path}folders/${folder}/`,
@@ -1432,8 +1436,8 @@ class AppDisplay extends BaseAppView {
             child.set_string('name', name);
             child.set_boolean('translate', true);
             child.set_strv('categories', categories);
-            if (apps)
-                child.set_strv('apps', apps);
+            if (filteredApps.length > 0)
+                child.set_strv('apps', filteredApps);
         }
     }
 
-- 
2.48.1

